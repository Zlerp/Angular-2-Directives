System.register(['@angular/core'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1;
    var AttributeDirectivesComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            AttributeDirectivesComponent = (function () {
                function AttributeDirectivesComponent() {
                }
                AttributeDirectivesComponent = __decorate([
                    core_1.Component({
                        selector: 'my-attribute-directives',
                        template: "\n    \n  ",
                    }), 
                    __metadata('design:paramtypes', [])
                ], AttributeDirectivesComponent);
                return AttributeDirectivesComponent;
            }());
            exports_1("AttributeDirectivesComponent", AttributeDirectivesComponent);
        }
    }
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImF0cnJpYnV0ZS5kaXJlY3RpdmVzLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztZQVNBO2dCQUFBO2dCQUVBLENBQUM7Z0JBVEQ7b0JBQUMsZ0JBQVMsQ0FBQzt3QkFDVCxRQUFRLEVBQUUseUJBQXlCO3dCQUNuQyxRQUFRLEVBQUUsWUFFVDtxQkFDRixDQUFDOztnREFBQTtnQkFJRixtQ0FBQztZQUFELENBRkEsQUFFQyxJQUFBO1lBRkQsdUVBRUMsQ0FBQSIsImZpbGUiOiJhdHJyaWJ1dGUuZGlyZWN0aXZlcy5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ215LWF0dHJpYnV0ZS1kaXJlY3RpdmVzJyxcclxuICB0ZW1wbGF0ZTogYFxyXG4gICAgXHJcbiAgYCxcclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBBdHRyaWJ1dGVEaXJlY3RpdmVzQ29tcG9uZW50IHtcclxuXHJcbn1cclxuIl0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
