System.register(["@angular/core"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, core_2, core_3;
    var HighlightDirective;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
                core_2 = core_1_1;
                core_3 = core_1_1;
            }],
        execute: function() {
            HighlightDirective = (function () {
                // this automatically bidns it to the privATE PROTERY IT IS CREATED AND SET HERE
                function HighlightDirective(_elRef, _renderer) {
                    this._elRef = _elRef;
                    this._renderer = _renderer;
                    this._defaultColor = 'green';
                }
                // when the directive gets initialised, the lifecycle hook will be called, and the code executed
                // ngOnInit():any {
                //   this._renderer.setElementStyle(this._elRef.nativeElement, 'background-color', this.highlightColor || this._defaultColor);
                //   // this._elRef.nativeElement.style.backgroundColor = this._defaultColor;
                // }
                HighlightDirective.prototype.onMouseEnter = function () {
                    this.highlight(this.highlightColor || this._defaultColor);
                };
                HighlightDirective.prototype.onMouseLeave = function () {
                    this.highlight(null);
                };
                HighlightDirective.prototype.highlight = function (color) {
                    this._renderer.setElementStyle(this._elRef.nativeElement, 'background-color', color);
                };
                HighlightDirective = __decorate([
                    core_1.Directive({
                        selector: '[myHighlight]',
                        inputs: ['highlightColor:myHighlight'],
                        // specify which events the hosting element that can be fired to be captured in the directive element.
                        host: {
                            '(mouseenter)': 'onMouseEnter()',
                            '(mouseleave)': 'onMouseLeave()'
                        }
                    }), 
                    __metadata('design:paramtypes', [core_2.ElementRef, core_3.Renderer])
                ], HighlightDirective);
                return HighlightDirective;
            }());
            exports_1("HighlightDirective", HighlightDirective);
        }
    }
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImhpZ2hsaWdodC5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztZQWVBO2dCQUtJLGdGQUFnRjtnQkFDaEYsNEJBQW9CLE1BQWtCLEVBQVUsU0FBbUI7b0JBQS9DLFdBQU0sR0FBTixNQUFNLENBQVk7b0JBQVUsY0FBUyxHQUFULFNBQVMsQ0FBVTtvQkFMN0Qsa0JBQWEsR0FBRyxPQUFPLENBQUM7Z0JBS3dDLENBQUM7Z0JBQ3ZFLGdHQUFnRztnQkFDaEcsbUJBQW1CO2dCQUNuQiw4SEFBOEg7Z0JBQzlILDZFQUE2RTtnQkFDN0UsSUFBSTtnQkFHSix5Q0FBWSxHQUFaO29CQUNJLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLGNBQWMsSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7Z0JBQzlELENBQUM7Z0JBRUQseUNBQVksR0FBWjtvQkFDRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUV2QixDQUFDO2dCQUVPLHNDQUFTLEdBQWpCLFVBQWtCLEtBQWE7b0JBQzdCLElBQUksQ0FBQyxTQUFTLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxFQUFFLGtCQUFrQixFQUFFLEtBQUssQ0FBQyxDQUFDO2dCQUN2RixDQUFDO2dCQW5DTDtvQkFBQyxnQkFBUyxDQUFDO3dCQUNULFFBQVEsRUFBRSxlQUFlO3dCQUN6QixNQUFNLEVBQUUsQ0FBQyw0QkFBNEIsQ0FBQzt3QkFDdEMsc0dBQXNHO3dCQUN0RyxJQUFJLEVBQUU7NEJBQ0osY0FBYyxFQUFDLGdCQUFnQjs0QkFDL0IsY0FBYyxFQUFFLGdCQUFnQjt5QkFDakM7cUJBQ0YsQ0FBQzs7c0NBQUE7Z0JBNEJGLHlCQUFDO1lBQUQsQ0ExQkEsQUEwQkMsSUFBQTtZQTFCRCxtREEwQkMsQ0FBQSIsImZpbGUiOiJoaWdobGlnaHQuZGlyZWN0aXZlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtEaXJlY3RpdmV9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7RWxlbWVudFJlZn0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHtPbkluaXR9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7UmVuZGVyZXJ9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcblxyXG5ARGlyZWN0aXZlKHtcclxuICBzZWxlY3RvcjogJ1tteUhpZ2hsaWdodF0nLFxyXG4gIGlucHV0czogWydoaWdobGlnaHRDb2xvcjpteUhpZ2hsaWdodCddLFxyXG4gIC8vIHNwZWNpZnkgd2hpY2ggZXZlbnRzIHRoZSBob3N0aW5nIGVsZW1lbnQgdGhhdCBjYW4gYmUgZmlyZWQgdG8gYmUgY2FwdHVyZWQgaW4gdGhlIGRpcmVjdGl2ZSBlbGVtZW50LlxyXG4gIGhvc3Q6IHtcclxuICAgICcobW91c2VlbnRlciknOidvbk1vdXNlRW50ZXIoKScsXHJcbiAgICAnKG1vdXNlbGVhdmUpJzogJ29uTW91c2VMZWF2ZSgpJ1xyXG4gIH1cclxufSlcclxuLy8gZXhwb3J0IGNsYXNzIEhpZ2hsaWdodERpcmVjdGl2ZSBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbmV4cG9ydCBjbGFzcyBIaWdobGlnaHREaXJlY3RpdmUge1xyXG4gIHByaXZhdGUgX2RlZmF1bHRDb2xvciA9ICdncmVlbic7XHJcblxyXG4gIGhpZ2hsaWdodENvbG9yOiBzdHJpbmc7XHJcblxyXG4gICAgLy8gdGhpcyBhdXRvbWF0aWNhbGx5IGJpZG5zIGl0IHRvIHRoZSBwcml2QVRFIFBST1RFUlkgSVQgSVMgQ1JFQVRFRCBBTkQgU0VUIEhFUkVcclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgX2VsUmVmOiBFbGVtZW50UmVmLCBwcml2YXRlIF9yZW5kZXJlcjogUmVuZGVyZXIpIHt9XHJcbiAgICAvLyB3aGVuIHRoZSBkaXJlY3RpdmUgZ2V0cyBpbml0aWFsaXNlZCwgdGhlIGxpZmVjeWNsZSBob29rIHdpbGwgYmUgY2FsbGVkLCBhbmQgdGhlIGNvZGUgZXhlY3V0ZWRcclxuICAgIC8vIG5nT25Jbml0KCk6YW55IHtcclxuICAgIC8vICAgdGhpcy5fcmVuZGVyZXIuc2V0RWxlbWVudFN0eWxlKHRoaXMuX2VsUmVmLm5hdGl2ZUVsZW1lbnQsICdiYWNrZ3JvdW5kLWNvbG9yJywgdGhpcy5oaWdobGlnaHRDb2xvciB8fCB0aGlzLl9kZWZhdWx0Q29sb3IpO1xyXG4gICAgLy8gICAvLyB0aGlzLl9lbFJlZi5uYXRpdmVFbGVtZW50LnN0eWxlLmJhY2tncm91bmRDb2xvciA9IHRoaXMuX2RlZmF1bHRDb2xvcjtcclxuICAgIC8vIH1cclxuXHJcblxyXG4gICAgb25Nb3VzZUVudGVyKCl7XHJcbiAgICAgICAgdGhpcy5oaWdobGlnaHQodGhpcy5oaWdobGlnaHRDb2xvciB8fCB0aGlzLl9kZWZhdWx0Q29sb3IpO1xyXG4gICAgfVxyXG5cclxuICAgIG9uTW91c2VMZWF2ZSgpe1xyXG4gICAgICB0aGlzLmhpZ2hsaWdodChudWxsKTtcclxuXHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBoaWdobGlnaHQoY29sb3I6IHN0cmluZykge1xyXG4gICAgICB0aGlzLl9yZW5kZXJlci5zZXRFbGVtZW50U3R5bGUodGhpcy5fZWxSZWYubmF0aXZlRWxlbWVudCwgJ2JhY2tncm91bmQtY29sb3InLCBjb2xvcik7XHJcbiAgICB9XHJcbn1cclxuIl0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
