System.register(['@angular/core', './unless.directive'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, unless_directive_1;
    var StructuralDirectives;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (unless_directive_1_1) {
                unless_directive_1 = unless_directive_1_1;
            }],
        execute: function() {
            StructuralDirectives = (function () {
                function StructuralDirectives() {
                    this.list = ['Apple', 'Milk', 'Bread'];
                }
                StructuralDirectives = __decorate([
                    core_1.Component({
                        selector: 'structural-directives',
                        template: "\n  <section class=\"directive\">\n    <h2>*ngIf</h2>\n    <div>\n      enter a number higher than 10\n      <br>\n      <input type=\"text\" #number (keyup)=\"0\">\n    </div>\n    <div *ngIf=\"number.value > 10\">\n      <h5>Hi</h5>\n      Number is greater than 10.\n    </div>\n  </section>\n  <section class=\"directive\">\n    <h2>*ngFor</h2>\n    <div>\n      <ul>\n        <li *ngFor=\"let item of list, #i = index\">{{item}} {{i}}</li>\n      </ul>\n    </div>\n  </section>\n  <section class=\"directive\">\n    <h2>[ngSwitch]</h2>\n    <div>\n      Enter red, blue or green:\n      <br>\n      <input type=\"text\" #color (keyup)=\"0\">\n    </div>\n    <div [ngSwitch]=\"color.value\">\n      <template [ngSwitchWhen]=\"'red'\"><span style=\"color: red\">It's Red</span></template>\n      <template [ngSwitchWhen]=\"'green'\"><span style=\"color: green\">It's green</span></template>\n      <template [ngSwitchWhen]=\"'blue'\"><span style=\"color: blue\">It's blue</span></template>\n      <template ngSwitchDefault><span>Don't know that color</span></template>\n    </div>\n  </section>\n  <section class=\"directive\">\n    <h2>Custom Directive</h2>\n    <div>\n      Enter true or false:\n      <br>\n      <input type=\"text\" #condition (keyup)=\"0\">\n    </div>\n    <div *myUnless=\"condition.value != 'false'\">\n      Only show if False.\n    </div>\n  </section>\n  ",
                        directives: [unless_directive_1.UnlessDirective]
                    }), 
                    __metadata('design:paramtypes', [])
                ], StructuralDirectives);
                return StructuralDirectives;
            }());
            exports_1("StructuralDirectives", StructuralDirectives);
        }
    }
});
// directives attached to an element that will change the structure of your element.

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInN0cnVjdHVyYWwtZGlyZWN0aXZlcy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7WUFxREE7Z0JBQUE7b0JBQ0ksU0FBSSxHQUFHLENBQUMsT0FBTyxFQUFFLE1BQU0sRUFBRSxPQUFPLENBQUMsQ0FBQTtnQkFDckMsQ0FBQztnQkFyREQ7b0JBQUMsZ0JBQVMsQ0FBQzt3QkFDVCxRQUFRLEVBQUUsdUJBQXVCO3dCQUNqQyxRQUFRLEVBQUUsKzJDQThDVDt3QkFDRCxVQUFVLEVBQUUsQ0FBQyxrQ0FBZSxDQUFDO3FCQUM5QixDQUFDOzt3Q0FBQTtnQkFHRiwyQkFBQztZQUFELENBRkEsQUFFQyxJQUFBO1lBRkQsdURBRUMsQ0FBQTs7OztBQUdELG9GQUFvRiIsImZpbGUiOiJzdHJ1Y3R1cmFsLWRpcmVjdGl2ZXMuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQge1VubGVzc0RpcmVjdGl2ZX0gZnJvbSAnLi91bmxlc3MuZGlyZWN0aXZlJ1xyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ3N0cnVjdHVyYWwtZGlyZWN0aXZlcycsXHJcbiAgdGVtcGxhdGU6IGBcclxuICA8c2VjdGlvbiBjbGFzcz1cImRpcmVjdGl2ZVwiPlxyXG4gICAgPGgyPipuZ0lmPC9oMj5cclxuICAgIDxkaXY+XHJcbiAgICAgIGVudGVyIGEgbnVtYmVyIGhpZ2hlciB0aGFuIDEwXHJcbiAgICAgIDxicj5cclxuICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgI251bWJlciAoa2V5dXApPVwiMFwiPlxyXG4gICAgPC9kaXY+XHJcbiAgICA8ZGl2ICpuZ0lmPVwibnVtYmVyLnZhbHVlID4gMTBcIj5cclxuICAgICAgPGg1PkhpPC9oNT5cclxuICAgICAgTnVtYmVyIGlzIGdyZWF0ZXIgdGhhbiAxMC5cclxuICAgIDwvZGl2PlxyXG4gIDwvc2VjdGlvbj5cclxuICA8c2VjdGlvbiBjbGFzcz1cImRpcmVjdGl2ZVwiPlxyXG4gICAgPGgyPipuZ0ZvcjwvaDI+XHJcbiAgICA8ZGl2PlxyXG4gICAgICA8dWw+XHJcbiAgICAgICAgPGxpICpuZ0Zvcj1cImxldCBpdGVtIG9mIGxpc3QsICNpID0gaW5kZXhcIj57e2l0ZW19fSB7e2l9fTwvbGk+XHJcbiAgICAgIDwvdWw+XHJcbiAgICA8L2Rpdj5cclxuICA8L3NlY3Rpb24+XHJcbiAgPHNlY3Rpb24gY2xhc3M9XCJkaXJlY3RpdmVcIj5cclxuICAgIDxoMj5bbmdTd2l0Y2hdPC9oMj5cclxuICAgIDxkaXY+XHJcbiAgICAgIEVudGVyIHJlZCwgYmx1ZSBvciBncmVlbjpcclxuICAgICAgPGJyPlxyXG4gICAgICA8aW5wdXQgdHlwZT1cInRleHRcIiAjY29sb3IgKGtleXVwKT1cIjBcIj5cclxuICAgIDwvZGl2PlxyXG4gICAgPGRpdiBbbmdTd2l0Y2hdPVwiY29sb3IudmFsdWVcIj5cclxuICAgICAgPHRlbXBsYXRlIFtuZ1N3aXRjaFdoZW5dPVwiJ3JlZCdcIj48c3BhbiBzdHlsZT1cImNvbG9yOiByZWRcIj5JdCdzIFJlZDwvc3Bhbj48L3RlbXBsYXRlPlxyXG4gICAgICA8dGVtcGxhdGUgW25nU3dpdGNoV2hlbl09XCInZ3JlZW4nXCI+PHNwYW4gc3R5bGU9XCJjb2xvcjogZ3JlZW5cIj5JdCdzIGdyZWVuPC9zcGFuPjwvdGVtcGxhdGU+XHJcbiAgICAgIDx0ZW1wbGF0ZSBbbmdTd2l0Y2hXaGVuXT1cIidibHVlJ1wiPjxzcGFuIHN0eWxlPVwiY29sb3I6IGJsdWVcIj5JdCdzIGJsdWU8L3NwYW4+PC90ZW1wbGF0ZT5cclxuICAgICAgPHRlbXBsYXRlIG5nU3dpdGNoRGVmYXVsdD48c3Bhbj5Eb24ndCBrbm93IHRoYXQgY29sb3I8L3NwYW4+PC90ZW1wbGF0ZT5cclxuICAgIDwvZGl2PlxyXG4gIDwvc2VjdGlvbj5cclxuICA8c2VjdGlvbiBjbGFzcz1cImRpcmVjdGl2ZVwiPlxyXG4gICAgPGgyPkN1c3RvbSBEaXJlY3RpdmU8L2gyPlxyXG4gICAgPGRpdj5cclxuICAgICAgRW50ZXIgdHJ1ZSBvciBmYWxzZTpcclxuICAgICAgPGJyPlxyXG4gICAgICA8aW5wdXQgdHlwZT1cInRleHRcIiAjY29uZGl0aW9uIChrZXl1cCk9XCIwXCI+XHJcbiAgICA8L2Rpdj5cclxuICAgIDxkaXYgKm15VW5sZXNzPVwiY29uZGl0aW9uLnZhbHVlICE9ICdmYWxzZSdcIj5cclxuICAgICAgT25seSBzaG93IGlmIEZhbHNlLlxyXG4gICAgPC9kaXY+XHJcbiAgPC9zZWN0aW9uPlxyXG4gIGAsXHJcbiAgZGlyZWN0aXZlczogW1VubGVzc0RpcmVjdGl2ZV1cclxufSlcclxuZXhwb3J0IGNsYXNzIFN0cnVjdHVyYWxEaXJlY3RpdmVze1xyXG4gICAgbGlzdCA9IFsnQXBwbGUnLCAnTWlsaycsICdCcmVhZCddXHJcbn1cclxuXHJcblxyXG4vLyBkaXJlY3RpdmVzIGF0dGFjaGVkIHRvIGFuIGVsZW1lbnQgdGhhdCB3aWxsIGNoYW5nZSB0aGUgc3RydWN0dXJlIG9mIHlvdXIgZWxlbWVudC5cclxuIl0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
