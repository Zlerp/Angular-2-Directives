import {Component} from '@angular/core';
import {AttributeDirectives} from './attribute-directives.component';
import {StructuralDirectives} from './structural-directives.component';

@Component({
    selector: 'my-app',
    template: `
      <my-attribute-directives></my-attribute-directives>
      <br>
      <h1>  Structural Directives </h1>
      <structural-directives></structural-directives>
    `,
    directives: [AttributeDirectives, StructuralDirectives]
})
export class AppComponent {

}
