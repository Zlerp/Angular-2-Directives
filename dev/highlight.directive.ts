import {Directive} from "@angular/core";
import {ElementRef} from "@angular/core";
import {OnInit} from "@angular/core";
import {Renderer} from "@angular/core";

@Directive({
  selector: '[myHighlight]',
  inputs: ['highlightColor:myHighlight'],
  // specify which events the hosting element that can be fired to be captured in the directive element.
  host: {
    '(mouseenter)':'onMouseEnter()',
    '(mouseleave)': 'onMouseLeave()'
  }
})
// export class HighlightDirective implements OnInit {
export class HighlightDirective {
  private _defaultColor = 'green';

  highlightColor: string;

    // this automatically bidns it to the privATE PROTERY IT IS CREATED AND SET HERE
    constructor(private _elRef: ElementRef, private _renderer: Renderer) {}
    // when the directive gets initialised, the lifecycle hook will be called, and the code executed
    // ngOnInit():any {
    //   this._renderer.setElementStyle(this._elRef.nativeElement, 'background-color', this.highlightColor || this._defaultColor);
    //   // this._elRef.nativeElement.style.backgroundColor = this._defaultColor;
    // }


    onMouseEnter(){
        this.highlight(this.highlightColor || this._defaultColor);
    }

    onMouseLeave(){
      this.highlight(null);

    }

    private highlight(color: string) {
      this._renderer.setElementStyle(this._elRef.nativeElement, 'background-color', color);
    }
}
